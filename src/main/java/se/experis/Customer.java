package se.experis;


public class Customer {
    private String firstName;
    private String lastName;
    private String genre;

    public Customer() {

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGenre() {
        return genre;
    }
}
