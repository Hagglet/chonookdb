package se.experis;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        searchMenu();
    }
    // control the flow if the menu
    public static void searchMenu() {
        Scanner sc = new Scanner(System.in);
        System.out.println("1: " + "Do a search manually" + "\n"
                + "2: " + "Do a search with a random number" +"\n"
                + "3: " + "Exit the program");
        int option = sc.nextInt();
        if (option == 1) {
           manualSearch();
        }
        if (option == 2) {
            randomSearch();
        }
        if(option == 3) {
            System.exit(1);
        }
    }
    // Menu for the manual search
    public static void manualSearch() {
        databaseHandler data = new databaseHandler();
        System.out.println("Enter a number between 1-59");
        Scanner sc = new Scanner(System.in);
        int searchId = sc.nextInt();
        if (searchId > 59) {
            System.out.println("You entered a to large number");
            manualSearch();
        } else if (searchId < 1) {
            System.out.println("You entered a to small number");
            manualSearch();
        }else {
            Customer customer = data.getInformation(searchId);
            System.out.println("Name: " +customer.getFirstName() + " " + customer.getLastName());
            System.out.println("Favorite genre: " +customer.getGenre());
            System.out.println();
            searchMenu();
        }
    }
    // menu for the random search
    public static void randomSearch() {
        databaseHandler data = new databaseHandler();
        Customer customer = data.getInformation(data.getRandomNumber());
        System.out.println("Name: " +customer.getFirstName() + " " + customer.getLastName());
        System.out.println("Favorite genre: " +customer.getGenre());
        System.out.println();
        searchMenu();
    }
}

