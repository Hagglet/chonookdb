package se.experis;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class databaseHandler {
    /*
     * Get the information from chinook database and will create a customer
     * The information that will be extracted is the customers Firstname, Lastname and favorite genre
     */
    public static Customer getInformation(int input) {
        String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
        Connection conn = null;
        String firstName ="";
        String lastName="";
        String genre = "";
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            PreparedStatement getName =
                    conn.prepareStatement("SELECT FirstName, LastName from Customer where CustomerId = ?");
            getName.setInt(1,input);

            PreparedStatement getMostPopularGenre =
                    conn.prepareStatement("SELECT Name from Genre where GenreId in (SELECT GenreId from Track where TrackId in (SELECT TrackId from InvoiceLine where InvoiceId in " +
                            "                               (SELECT InvoiceId from Invoice WHERE CustomerId=?)) GROUP BY GenreId order by COUNT(GenreId) DESC limit 1)");
            getMostPopularGenre.setInt(1,input);

            ResultSet resultSetGenre = getMostPopularGenre.executeQuery();
            ResultSet resultSetName = getName.executeQuery();
            while (resultSetName.next()) {
                firstName = resultSetName.getString("FirstName");
                lastName = resultSetName.getString("LastName");

            }
            while (resultSetGenre.next()) {
                genre = resultSetGenre.getString("Name");

            }
            customer = new Customer();
            customer.setFirstName(firstName);
            customer.setLastName(lastName);
            customer.setGenre(genre);

        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return customer;
    }
    /*
     * Will generate a random number based on the number of customers that exist in the database
     * In this case a number between 1-59
     */
    public int getRandomNumber() {
        String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
        int id = 0;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement getSize = conn.prepareStatement("SELECT COUNT(CustomerId) from Customer");
            ResultSet resultSetgetSize = getSize.executeQuery();
            resultSetgetSize.next();
            int size = resultSetgetSize.getInt(1);
            id = (int) (Math.random() * size);
        } catch (Exception e) {

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        System.out.println(id);
        return id;
    }
}
